<?php

namespace GuanChanghu\Traits\SafeOperation;

use Closure;

/**
 * @author 管昌虎
 * Trait Facade
 * @method mixed safeOperation(Closure $closure, bool $isTransaction = true, bool $isThrowable = true)      安全执行可以在定义throwable方法记录日志
 * @package GuanChanghu\Traits\SafeOperation
 * Created on 2023/4/3 20:43
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
trait Facade
{
    use Encrypt;
}
