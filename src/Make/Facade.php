<?php

namespace GuanChanghu\Traits\Make;


/**
 * @author 管昌虎
 * Trait Facade
 * @method static static make(...$parameter)
 * @package GuanChanghu\Traits\Make
 * Created on 2023/4/22 20:21
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
trait Facade
{
    use Encrypt;
}