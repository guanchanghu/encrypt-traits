<?php

namespace GuanChanghu\Traits\TimeRange;


/**
 * 开始时间结束时间必须标注datetime/date
 * 开始时间字段started_at
 * 结束时间字段ended_at
 * @author 管昌虎
 * Trait Facade
 * @method static bool isEnabled(string $startedAt, string $endedAt)    是否开启
 * @method int timeStatus()                                             时间状态-10:未开始;20-已开始;30-已结束;
 * @method string getTimeStatusLabelAttribute()                         获得时间状态label
 * @method bool checkStatus(string $key = 'common.time.expire')         检查状态是否结束，会抛出错误UserException
 * @package GuanChanghu\Traits\TimeRange
 * Created on 2023/4/4 21:27
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
trait Facade
{
    use Encrypt;
}
