<?php

namespace GuanChanghu\Traits\Log;


/**
 * @author 管昌虎
 * Class Facade
 * @method static string|null getLogChannel(string $client = '', bool $isSlowLog = false, string $channel = '')     获得日志channel
 * @package GuanChanghu\Traits\Log
 * Created on 2023/4/5 12:49
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
trait Facade
{
    use Encrypt;
}
