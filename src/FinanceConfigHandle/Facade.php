<?php

namespace GuanChanghu\Traits\FinanceConfigHandle;


use Illuminate\Support\Collection;

/**
 * @author 管昌虎
 * Trait Facade
 * @method static array getCharge(Collection $config, float $amount)                    获得手续费，实际金额，转正金额
 * @method static void checkAmount(Collection $config, float $amount)                   检查金额，最小，最大，倍数
 * @method static void checkWallet(Collection $config, int $wallet)                     检查钱包
 * @method static float cnyToWallet(float $amount, float $ratio)                        人民币转钱包
 * @method static float walletToCny(float $amount, float $ratio)                        钱包转人民币
 * @method static float walletConvert(float $amount, float $fromRatio, float $ratio)    钱包转换
 * @package GuanChanghu\Traits\FinanceConfigHandle
 * Created on 2023/4/4 22:23
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
trait Facade
{
    use Encrypt;
}
