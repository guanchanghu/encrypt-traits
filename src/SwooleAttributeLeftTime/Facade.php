<?php
/**
 * swoole属性生存时间门面
 * Created on 2022/3/23 8:56
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Traits\SwooleAttributeLeftTime;

use Illuminate\Support\Collection;
use Closure;

/**
 * Trait Facade
 * @method array|string|Collection getSwooleGarrisonAttribute(string $attribute, int $seconds, Closure $callback)       存储变量
 * @method array|string|Collection clearSwooleGarrisonAttribute(string $attribute)          清除存储变量
 * @package GuanChanghu\Traits\SwooleAttributeLeftTime
 * Created on 2022/3/23 8:56
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
trait Facade
{
    use Encrypt;
}
