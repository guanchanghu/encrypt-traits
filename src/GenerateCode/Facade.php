<?php

namespace GuanChanghu\Traits\GenerateCode;


/**
 * @author 管昌虎
 * Trait Facade
 * @method static string generateCode(string $column = 'code', int $length = 8)                     生成code
 * @method static string getCodeById(int $id, string $column = 'code')                              获得code数据
 * @method static int getIdByCode(string $code, string $column = 'code')                            获得主键根据code
 * @method static static getDetailByCode(string $code, string $column = 'code', int $options = 0)   获得详情根据code
 * @package GuanChanghu\Traits\GenerateCode
 * Created on 2023/4/3 20:48
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
trait Facade
{
    use Encrypt;
}
