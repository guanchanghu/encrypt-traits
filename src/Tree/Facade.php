<?php

namespace GuanChanghu\Traits\Tree;


use Illuminate\Support\Collection;

/**
 * @author 管昌虎
 * Trait Facade
 * @method array treePath(string $parentIdField = 'parentId', string $nameField = 'name')           此方法不能获取多语言的路径，上级路径
 * @method static Collection treePathMany(Collection $items, string $parentIdField = 'parentId', string $nameField = 'name', array $columnListing = [])     (支持多语言)可以获取的路径
 * @method static Collection treePathByManyId(Collection $idMany, string $parentIdField = 'parentId', string $nameField = 'name')               (支持多语言)可以获取的路径 id
 * @package GuanChanghu\Traits\Tree
 * Created on 2023/4/3 22:23
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
trait Facade
{
    use Encrypt;
}
