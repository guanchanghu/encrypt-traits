<?php


namespace GuanChanghu\Traits\Cache;


use Closure;
use Illuminate\Cache\TaggedCache;

/**
 * Trait Facade
 * @method static lock(string $lockName, Closure $successCallback, Closure|string $failCallback, int $waitSeconds = 0, int $runSeconds = 0)   加锁
 * @method static unlock(string $lockName = '')                 无条件释放锁
 * @method static string wholeLockName(string $lockName = '')   获得加锁名称
 * @method static TaggedCache tags(array $tags = [])            获得标签cache
 * @method static bool forget(string $key, array $tags = [])    删除特定缓存，带有tags
 * @method static bool flush(array $tags = [])                  方法清空所有的缓存
 * @package GuanChanghu\Traits\Cache
 */
trait Facade
{
    use Encrypt;
}
