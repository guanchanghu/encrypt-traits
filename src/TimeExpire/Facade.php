<?php

namespace GuanChanghu\Traits\TimeExpire;


/**
 * @author 管昌虎
 * Trait Facade
 * @method static bool isEnabled(string $expiredAt)             是否开启
 * @method int timeStatus()                                     事件状态-10:未开始;20-已开始;30-已结束;
 * @package GuanChanghu\Traits\TimeExpire
 * Created on 2023/4/4 22:11
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
trait Facade
{
    use Encrypt;
}
